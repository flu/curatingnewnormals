
# Description and planning
## Faculty

Daniel Charny

## Syllabus and Learning Objectives

Curating New Normals will support participants in exploring, expanding and expressing their work on emergent futures. Starting from a personal position and progressing into a collective vision they will build an engaging narrative around change using a curatorial process. The course will ask designers to explore a broad scope of connections, associations and references to contextualise their own work in order to communicate their area of interest, issues, type of change and overarching message. Through a co-creative process they will then explore synergies with others it order to merge and evolve a joint proposal. During this process they will identify themes and reframe their work into a collective narrative, understand audiences and interrogate appropriate formats.

The course is modelled on a design process that draws on curatorial practice. Learning through productive processes and using it to generate knowledge. Discussion of curatorial approaches will include exploring subjects through scripting, visualisation and materialisation. It will touch on: curatorial concepts, thematic and narrative structures, engagement, Interpretation, gateway exhibits, key messages, stake holder and audience development, issues of environment and media, and particular rethinking programmes and formats.

Above all the aim of the course is to clarify the individual projects and to enhance the understanding of their ambition and role in a bigger picture.

Examples and tools will be drawn from the hybrid practice of From Now On creative strategic consultancy.

## Total Duration

Bi-weekly, on Friday from 14:30hrs to 17:30hrs (CET).

## Structure and Phases
    1- Curate personal work in wider context
    2- Broaden scope, identify themes, gateways and narrative order.Start merge.
    3- Merge phase 1 - key messages, refining sub themes, key audiences, over arching theme.
    4- Merge phase 2 - reframe, refine selection, themes, messaging, titles, key images.
    5- Who, How and Where - Collective project on audiences, format and programme.

## Output

TBC

## Grading Method

Personal work presentation 20%.
Merge phase presentation 30%.
Collaborative work 40%.

## Bibliography

Three Amigos - What Is A Plethora?

MORE ABOUT THE SEMINAR:  https://mdef.gitlab.io/mdef-2019/term3/S03.html
