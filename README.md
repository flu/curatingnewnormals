Curating New Normals
=====================

Curating New Normals will support participants in exploring, expanding and expressing their work on emergent futures. Starting from a personal position and progressing into a collective vision they will build an engaging narrative around...

You will find more information about the class here: https://mdef.gitlab.io/mdef-2019/
